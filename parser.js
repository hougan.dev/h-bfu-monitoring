const axios = require('axios');
const DomParser = require('dom-parser');

class Parser {
    /** Базовая ссылка */
    static baseUri = 'http://abitstat.kantiana.ru';
    /** Ссылка на список направлений */
    static classesUri = '/RatingList/Professions/6eeb03b3-cd87-4921-b947-2246c75365c5';

    /**
     * Получить список направлений для подготовки
     */
    static async getClasses() {
        const { data: html } = await axios.get(Parser.baseUri + Parser.classesUri);
        
        const dom = Parser.buildDom(html);
        const result = dom.getElementsByTagName('tr').map(line => {
            const objects = line.getElementsByTagName('td');
            if (!objects || objects.length != 2) {
                return null;
            }

            return !objects || objects.length != 2 ? null :  {
                name: objects[1].innerHTML,
                uri: line.getAttribute('onclick')
                    .replace('\'', '')
                    .replace('\'', '')
                    .replace('location.href=', Parser.baseUri)
            };
        }).filter(v => v);

        return result;
    }

    /**
     * Найти пользователя в классе
     * @param {any} classInfo
     */
    static async findInClass(classInfo, firstName, secondName, thirdName) {
        const start = new Date();
        const { data: html } = await axios.get(classInfo.uri);

        const dom = Parser.buildDom(html);
        const result = dom.getElementsByTagName('tr').map(line => {
            const objects = line.getElementsByTagName('td');

            if (!objects || objects.length < 8) {
                return null;
            }

            return {
                id: objects[0].innerHTML,

                name: `${objects[3].innerHTML} ${objects[2].innerHTML} ${objects[4].innerHTML}`,
                type: classInfo.name,
                time: new Date().getTime() - start
            }
        }).filter(v => v);

        return result.find(v => {
            const name = v.name.toLowerCase();
            if (name.indexOf(firstName) !== -1 && name.indexOf(secondName) !== -1) {
                return true;
            }

            return false;
        });
    }


    /**
     * Построить ДОМ по входящей строке
     * @param {string} input 
     */
    static buildDom(input) {
        const parser = new DomParser();

        return parser.parseFromString(input);
    }
}

module.exports = Parser;