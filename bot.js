const vkBot = require('node-vk-bot-api');
const Markup = require('node-vk-bot-api/lib/markup');
const Session = require('node-vk-bot-api/lib/session');

const { bot: { token } }= require('./config');
const Parser = require('./parser');
const session = new Session();

class Bot {
    /**
     * Конструктор класса
     * @param {any} value 
     */
    constructor(value) {
        this.data = value;

        // Инициализируем бота
        this.initialize();
    }

    /**
     * Инстант рабочего бота
     */
    get bot() { return this.data.bot; }

    set bot(v) { this.data.bot = v; }

    /**
     * Токен от бота
     * @type {string}
     */
    get token() { return this.data.token; }

    /**
     * Доступные кнопки для бота
     * @returns {any[]}
     */
    getButtons() {
        return [
            [
                {
                    name: 'Какой я в рейтинге?',
                    color: 'primary',
                    action: async (ctx) => {
                        const { from_id } = ctx.message;

                        let name = await this.getNameFromCtx(from_id.toString());

                        this.fetchRating(ctx, name);
                    }
                }
            ]
        ]
    }

    getMessages() {
        return [
            'Всё ещё обновляем информацию 😝',
            'Осталось совсем немного! 😎',
            'Потерпите пожалуйста 😪'
        ]
    }

    /**
     * Получить клавиатуру с возможностями
     */
    getKeyboard() {
        return this.getButtons()
            .map(line => {
                return line
                    .map(button => {
                        return Markup.button(button.name, button.color);
                    })
            })
    }

    /**
     * Инициализация бота
     */
    async initialize() {
        this.bot = new vkBot(this.token);
        this.bot.use(session.middleware());

        this.subCommands();

        this.bot.startPolling();

        console.log('Бот запущен в штатном режиме!');
    }

    /**
     * Метод подписывающий бота на команды 
     */
    async subCommands() {
        this.bot.command('/start', (ctx) => this.sendMenu(ctx));

        this.getButtons().forEach(line => {
            line.forEach(value => {
                console.log(`Зарегистрирована команда: '${value.name}'`);
                this.bot.command(value.name, value.action);
            });
        });

        this.bot.on(async (ctx) => {
            if (!ctx.session.fail) {
                await ctx.reply('Я всего лишь подсказываю твой рейтинг, чего ты хочешь?');
                return;
            }

            ctx.session.fail = false;
            await this.fetchRating(ctx, ctx.message.text);
        });

    }

    /**
     * Отправить меню пользователю
     * @param {any} ctx
     */
    async sendMenu(ctx) {
        ctx.reply('Вот, чем я могу тебе помочь!', null, Markup.keyboard(this.getKeyboard()));
    }

    /**
     * Проверить позицию пользователя в рейтинге
     * @param {any} ctx 
     * @param {string} name  
     */
    async fetchRating(ctx, name) {
        const classes = await Parser.getClasses();
        const start = new Date();
        const result = [];

        await ctx.reply('Подождите, обновляем информацию! 😇');
        const interval = setInterval(() => {
            const messages = this.getMessages();

            const message = messages[Math.floor(Math.random() * messages.length - 1)];
            ctx.reply(message);
        }, 10000);

        let promises = classes.map(value => Parser.findInClass(value, ...name.toLowerCase().split(' ')));
        Promise.all(promises).then(async infos => {
            clearInterval(interval);

            infos = infos.filter(v => v);
            if (infos.length == 0) {
                await ctx.reply(`Мы не нашли абитуриента с именем "${name}", отправьте нам своё настоящее ФИО. Также, как вы записаны в рейтинге!
                    Например: Хуганов Владимир Олегович`, 'photo258905623_457256482');

                ctx.session.fail = true;
                return;
            }
 

            for (let i = 0; i < infos.length; i++) {
                const info = infos[i];
    
                if (!info || !info.id) { 
                    continue;
                }
    
               await ctx.reply(`${info.type}`, null, Markup.keyboard([
                    [
                        Markup.button(`${info.id} место`, 'positive'),
                        Markup.button(`${info.time + new Date().getTime() - start}ms`, 'default')
                    ]
                ]).inline());
            }
        });



    }

    /**
     * Получить информацию о пользователе по CTX
     * @param {number} user_id
     * @returns {string}
     */
    async getNameFromCtx(user_id) {
        const result = await this.bot.execute('users.get', {
            user_ids: user_id
        });

        if (!result || result.length == 0) {
            return 'UNKNOWN';
        }

        const { first_name, last_name } = result[0];

        return `${first_name} ${last_name}`;
    }
}

module.exports = Bot;